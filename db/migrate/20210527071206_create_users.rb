class CreateUsers < ActiveRecord::Migration[6.1]
  def change
    create_table :users do |t|
      t.text :username
      t.text :email
      t.text :password
      t.boolean :has_verified

      t.timestamps
    end
  end
end
