class User < ApplicationRecord
  include Clearance::User

  has_many :articles, dependent: :destroy
  has_many :comments, dependent: :destroy, through: :articles
end
